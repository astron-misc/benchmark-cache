CXX=icpc
CFLAGS=-qopenmp -O3 -xHost

default: main-avx2.x main-avx512.x

main-avx2.x: main.c common.h sp_kernels-avx2.o dp_kernels-avx2.o int_kernels-avx2.o
	$(CXX) $^ -o $@ ${CFLAGS} -DUSE_AVX2

%_kernels-avx2.o: %_kernels.c
	$(CXX) -c $^ -o $@ ${CFLAGS} -g -DUSE_AVX2
	$(CXX) -c $^ -o $@.s ${CFLAGS} -S -DUSE_AVX2

main-avx512.x: main.c common.h sp_kernels-avx512.o dp_kernels-avx512.o int_kernels-avx512.o
	$(CXX) $^ -o $@ ${CFLAGS} -DUSE_AVX512

%_kernels-avx512.o: %_kernels.c
	$(CXX) -c $^ -o $@ ${CFLAGS} -g -DUSE_AVX512
	$(CXX) -c $^ -o $@.s ${CFLAGS} -S -DUSE_AVX512

tar:
	$(eval COMMIT := $(shell git rev-parse HEAD | cut -c 1-8))
	$(eval NAME := $(shell basename ${PWD}))
	tar -cf ${NAME}-${COMMIT}.tar *.c *h Makefile

clean:
	@rm -f *.o *.s main.x
