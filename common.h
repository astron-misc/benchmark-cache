#include <immintrin.h>
#include <omp.h>

#define NR_ITERATIONS 1
#define COUNT         1600000ULL
#define FMA           2

#ifdef USE_AVX2
#define ALIGNMENT     32
#define VECTOR_LENGTH 8
#define vector_ps __m256
#define vector_pd __m256d
#define set_ps    _mm256_set_ps(1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f)
#define set_pd    _mm256_set_pd(1.0f, 1.0f, 1.0f, 1.0f)
#define set_epi   _mm256_set_ps(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0)
#endif

#ifdef USE_AVX512
#define ALIGNMENT     64
#define VECTOR_LENGTH 16
#define vector_ps __m512
#define vector_pd __m512d
#define set_ps    _mm512_set1_ps(1.0f);
#define set_pd    _mm512_set1_pd(1.0f);
#define set_int   _mm512_set1_epi(1.0);
#endif

#define LOAD_16 LOAD_8  LOAD_8
#define LOAD_32 LOAD_16 LOAD_16
#define LOAD_64 LOAD_32 LOAD_32

#define FMA_16 FMA_8  FMA_8
#define FMA_32 FMA_16 FMA_16
#define FMA_64 FMA_32 FMA_32

#define ADD_16 ADD_8  ADD_8
#define ADD_32 ADD_16 ADD_16
#define ADD_64 ADD_32 ADD_32

#define COMBINE_FMA_LOAD 0
