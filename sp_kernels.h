#include "common.h"

void kernel_ps_1024_8(void *ptr);
void kernel_ps_512_8(void *ptr);
void kernel_ps_256_8(void *ptr);
void kernel_ps_128_8(void *ptr);
void kernel_ps_64_8(void *ptr);
void kernel_ps_32_8(void *ptr);
void kernel_ps_16_8(void *ptr);
void kernel_ps_8_8(void *ptr);
void kernel_ps_8_16(void *ptr);
void kernel_ps_8_32(void *ptr);
void kernel_ps_8_64(void *ptr);
void kernel_ps_8_128(void *ptr);
void kernel_ps_8_256(void *ptr);
