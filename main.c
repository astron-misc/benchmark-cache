#include <iostream>
#include <string>
#include <iomanip>

#include <stdio.h>
#include <omp.h>

using namespace std;

#include "common.h"
#include "sp_kernels.h"
#include "dp_kernels.h"
#include "int_kernels.h"


void report(string name, double seconds, double gflops) {
    int w1 = 8;
    int w2 = 8;
    cout << setw(w1) << string(name) << ": ";
    cout << setprecision(2) << fixed;
    cout << setw(w2) << seconds * 1e3 << " ms";
    if (gflops != 0)
        cout << ", " << setw(w2) << gflops / seconds / 1e9 << " GFlops/s";
    cout << endl;
}

void log(double ratio, double seconds, double gflops) {
    int w1 = 8;
    cout << setprecision(8) << fixed;
    cout << setw(w1) << ratio << "\t";
    cout << setprecision(2) << fixed;
    cout << gflops / seconds / 1e9;
    cout << endl;
}

double run_kernel(void (*kernel)(void *), void *ptr) {
    // Warmup
    kernel(ptr);

    // Benchmark
    double runtime = -omp_get_wtime();
    for (int i = 0; i < NR_ITERATIONS; i++) {
        kernel(ptr);
    }
    runtime += omp_get_wtime();

    // Finish measurement
    return runtime / NR_ITERATIONS;
}


void run_ps() {
    clog << "single precision" << endl;
    double milliseconds;
    float ratio;
    double flops = COUNT*8*VECTOR_LENGTH*FMA;
    long size = 8*VECTOR_LENGTH*sizeof(float);
    void *ptr = aligned_alloc(ALIGNMENT, size);

    // Warmup
    milliseconds = run_kernel(kernel_ps_8_8, ptr);

    // Benchmark
    milliseconds = run_kernel(kernel_ps_1024_8, ptr);
    ratio = 0.5 * (float) 8 / 1024;
    log(ratio, milliseconds, flops);

    milliseconds = run_kernel(kernel_ps_512_8, ptr);
    ratio = 0.5 * (float) 8 / 512;
    log(ratio, milliseconds, flops*2);

    milliseconds = run_kernel(kernel_ps_256_8, ptr);
    ratio = 0.5 * (float) 8 / 256;
    log(ratio, milliseconds, flops*4);

    milliseconds = run_kernel(kernel_ps_128_8, ptr);
    ratio = 0.5 * (float) 8 / 128;
    log(ratio, milliseconds, flops*8);

    milliseconds = run_kernel(kernel_ps_64_8, ptr);
    ratio = 0.5 * (float) 8 / 64;
    log(ratio, milliseconds, flops*16);

    milliseconds = run_kernel(kernel_ps_32_8, ptr);
    ratio = 0.5 * (float) 8 / 32;
    log(ratio, milliseconds, flops*32);

    milliseconds = run_kernel(kernel_ps_16_8, ptr);
    ratio = 0.5 * (float) 8 / 16;
    log(ratio, milliseconds, flops*64);

    milliseconds = run_kernel(kernel_ps_8_8, ptr);
    ratio = 0.5 * (float) 8 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_ps_8_16, ptr);
    ratio = 0.5 * (float) 16 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_ps_8_32, ptr);
    ratio = 0.5 * (float) 32 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_ps_8_64, ptr);
    ratio = 0.5 * (float) 64 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_ps_8_128, ptr);
    ratio = 0.5 * (float) 128 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_ps_8_256, ptr);
    ratio = 0.5 * (float) 256 / 8;
    log(ratio, milliseconds, flops*128);

    free(ptr);
    clog << endl;
}

void run_pd() {
    clog << "double precision" << endl;
    double milliseconds;
    float ratio;
    double flops = COUNT*4*VECTOR_LENGTH*FMA;
    long size = 8*VECTOR_LENGTH*sizeof(double);
    void *ptr = aligned_alloc(ALIGNMENT, size);

    // Warmup
    milliseconds = run_kernel(kernel_pd_8_8, ptr);

    // Benchmark
    milliseconds = run_kernel(kernel_pd_1024_8, ptr);
    ratio = 0.25 * (float) 8 / 1024;
    log(ratio, milliseconds, flops);

    milliseconds = run_kernel(kernel_pd_512_8, ptr);
    ratio = 0.25 * (float) 8 / 512;
    log(ratio, milliseconds, flops*2);

    milliseconds = run_kernel(kernel_pd_256_8, ptr);
    ratio = 0.25 * (float) 8 / 256;
    log(ratio, milliseconds, flops*4);

    milliseconds = run_kernel(kernel_pd_128_8, ptr);
    ratio = 0.25 * (float) 8 / 128;
    log(ratio, milliseconds, flops*8);

    milliseconds = run_kernel(kernel_pd_64_8, ptr);
    ratio = 0.25 * (float) 8 / 64;
    log(ratio, milliseconds, flops*16);

    milliseconds = run_kernel(kernel_pd_32_8, ptr);
    ratio = 0.25 * (float) 8 / 32;
    log(ratio, milliseconds, flops*32);

    milliseconds = run_kernel(kernel_pd_16_8, ptr);
    ratio = 0.25 * (float) 8 / 16;
    log(ratio, milliseconds, flops*64);

    milliseconds = run_kernel(kernel_pd_8_8, ptr);
    ratio = 0.25 * (float) 8 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_pd_8_16, ptr);
    ratio = 0.25 * (float) 16 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_pd_8_32, ptr);
    ratio = 0.25 * (float) 32 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_pd_8_64, ptr);
    ratio = 0.25 * (float) 64 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_pd_8_128, ptr);
    ratio = 0.25 * (float) 128 / 8;
    log(ratio, milliseconds, flops*128);

    milliseconds = run_kernel(kernel_pd_8_256, ptr);
    ratio = 0.25 * (float) 256 / 8;
    log(ratio, milliseconds, flops*128);

    free(ptr);
    clog << endl;
}

void run_int() {
    double milliseconds;
    double flops = COUNT*8*VECTOR_LENGTH;
    long size = 8*VECTOR_LENGTH*sizeof(float);
    void *ptr = aligned_alloc(ALIGNMENT, size);

    // Warmup
    milliseconds = run_kernel(kernel_int_8_8, ptr);

    // Benchmark
    milliseconds = run_kernel(kernel_int_1024_8, ptr);
    report("int 128:1", milliseconds, flops/8);

    milliseconds = run_kernel(kernel_int_512_8, ptr);
    report("int  64:1", milliseconds, flops/4);

    milliseconds = run_kernel(kernel_int_256_8, ptr);
    report("int  32:1", milliseconds, flops/2);

    milliseconds = run_kernel(kernel_int_128_8, ptr);
    report("int  16:1", milliseconds, flops);

    milliseconds = run_kernel(kernel_int_64_8, ptr);
    report("int   8:1", milliseconds, flops);

    milliseconds = run_kernel(kernel_int_32_8, ptr);
    report("int   4:1", milliseconds, flops);

    milliseconds = run_kernel(kernel_int_16_8, ptr);
    report("int   2:1", milliseconds, flops);

    milliseconds = run_kernel(kernel_int_8_8, ptr);
    report("int   1:1", milliseconds, flops);

    milliseconds = run_kernel(kernel_int_8_16, ptr);
    report("int   1:2", milliseconds, 2*flops);

    milliseconds = run_kernel(kernel_int_8_32, ptr);
    report("int   1:4", milliseconds, 4*flops);

    milliseconds = run_kernel(kernel_int_8_64, ptr);
    report("int   1:8", milliseconds, 8*flops);

    milliseconds = run_kernel(kernel_int_8_128, ptr);
    report("int  1:16", milliseconds, 8*flops);

    milliseconds = run_kernel(kernel_int_8_256, ptr);
    report("int  1:32", milliseconds, 8*flops);

    free(ptr);
}

int main(int argc, char **argv) {
    run_ps();
    run_pd();
    //run_int();

    return 0;
}
