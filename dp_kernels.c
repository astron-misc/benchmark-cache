#include "dp_kernels.h"

#define INIT \
    vector_pd a, b, c, d, e, f, g, h; \
    a = set_pd; \
    b = set_pd; \
    c = set_pd; \
    d = set_pd; \
    e = set_pd; \
    f = set_pd; \
    g = set_pd; \
    h = set_pd; \
    vector_pd m0, m1, m2, m3, m4, m5, m6, m7; \
    double* m0_ptr = (double *) ptr + 0 * VECTOR_LENGTH; \
    double* m1_ptr = (double *) ptr + 1 * VECTOR_LENGTH; \
    double* m2_ptr = (double *) ptr + 2 * VECTOR_LENGTH; \
    double* m3_ptr = (double *) ptr + 3 * VECTOR_LENGTH; \
    double* m4_ptr = (double *) ptr + 4 * VECTOR_LENGTH; \
    double* m5_ptr = (double *) ptr + 5 * VECTOR_LENGTH; \
    double* m6_ptr = (double *) ptr + 6 * VECTOR_LENGTH; \
    double* m7_ptr = (double *) ptr + 7 * VECTOR_LENGTH;

#define LOAD_8 \
    asm ("vmovapd %1, %0" : "=x" (m0) : "m" (*m0_ptr)); \
    asm ("vmovapd %1, %0" : "=x" (m1) : "m" (*m1_ptr)); \
    asm ("vmovapd %1, %0" : "=x" (m2) : "m" (*m2_ptr)); \
    asm ("vmovapd %1, %0" : "=x" (m3) : "m" (*m3_ptr)); \
    asm ("vmovapd %1, %0" : "=x" (m4) : "m" (*m4_ptr)); \
    asm ("vmovapd %1, %0" : "=x" (m5) : "m" (*m5_ptr)); \
    asm ("vmovapd %1, %0" : "=x" (m6) : "m" (*m6_ptr)); \
    asm ("vmovapd %1, %0" : "=x" (m7) : "m" (*m7_ptr)); \

#define STORE_8 \
    asm ("vmovntpd %1, %0" : "=m" (*m0_ptr) : "x" (m0)); \
    asm ("vmovntpd %1, %0" : "=m" (*m1_ptr) : "x" (m1)); \
    asm ("vmovntpd %1, %0" : "=m" (*m2_ptr) : "x" (m2)); \
    asm ("vmovntpd %1, %0" : "=m" (*m3_ptr) : "x" (m3)); \
    asm ("vmovntpd %1, %0" : "=m" (*m4_ptr) : "x" (m4)); \
    asm ("vmovntpd %1, %0" : "=m" (*m5_ptr) : "x" (m5)); \
    asm ("vmovntpd %1, %0" : "=m" (*m6_ptr) : "x" (m6)); \
    asm ("vmovntpd %1, %0" : "=m" (*m7_ptr) : "x" (m7)); \

#define FMA_8 \
    asm ("vfmadd213pd %1, %1, %0" :  "=x" (a) : "x" (m0), "0" (a)); \
    asm ("vfmadd213pd %1, %1, %0" :  "=x" (b) : "x" (m1), "0" (b)); \
    asm ("vfmadd213pd %1, %1, %0" :  "=x" (c) : "x" (m2), "0" (c)); \
    asm ("vfmadd213pd %1, %1, %0" :  "=x" (d) : "x" (m3), "0" (d)); \
    asm ("vfmadd213pd %1, %1, %0" :  "=x" (e) : "x" (m4), "0" (e)); \
    asm ("vfmadd213pd %1, %1, %0" :  "=x" (f) : "x" (m5), "0" (f)); \
    asm ("vfmadd213pd %1, %1, %0" :  "=x" (g) : "x" (m6), "0" (g)); \
    asm ("vfmadd213pd %1, %1, %0" :  "=x" (h) : "x" (m7), "0" (h));

#if COMBINE_FMA_LOAD
#define FMA_LOAD_8 \
    asm ("vfmadd213pd %2, %0, %1" :  "=x" (a) : "0" (a), "m" (*m0_ptr)); \
    asm ("vfmadd213pd %2, %0, %1" :  "=x" (b) : "0" (b), "m" (*m1_ptr)); \
    asm ("vfmadd213pd %2, %0, %1" :  "=x" (c) : "0" (c), "m" (*m2_ptr)); \
    asm ("vfmadd213pd %2, %0, %1" :  "=x" (d) : "0" (d), "m" (*m3_ptr)); \
    asm ("vfmadd213pd %2, %0, %1" :  "=x" (e) : "0" (e), "m" (*m4_ptr)); \
    asm ("vfmadd213pd %2, %0, %1" :  "=x" (f) : "0" (f), "m" (*m5_ptr)); \
    asm ("vfmadd213pd %2, %0, %1" :  "=x" (g) : "0" (g), "m" (*m6_ptr)); \
    asm ("vfmadd213pd %2, %0, %1" :  "=x" (h) : "0" (h), "m" (*m7_ptr));
#else
#define FMA_LOAD_8 \
    LOAD_8 \
    FMA_8
#endif

void kernel_pd_1024_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            LOAD_64 LOAD_64 LOAD_64 LOAD_64
            LOAD_64 LOAD_64 LOAD_64 LOAD_64
            LOAD_64 LOAD_64 LOAD_64 LOAD_64
            LOAD_64 LOAD_64
			LOAD_64 LOAD_32
			LOAD_16 LOAD_8
            FMA_LOAD_8
        }
        STORE_8
    }
}
void kernel_pd_512_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*2; i++) {
            LOAD_64 LOAD_64
            LOAD_64 LOAD_64
            LOAD_64 LOAD_64
            LOAD_64 LOAD_32
			LOAD_16	LOAD_8
            FMA_LOAD_8
        }
        STORE_8
    }
}

void kernel_pd_256_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*4; i++) {
            LOAD_64 LOAD_64
            LOAD_64 LOAD_32
			LOAD_16	LOAD_8
            FMA_LOAD_8
        }
        STORE_8
    }
}

void kernel_pd_128_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*8; i++) {
            LOAD_64
            LOAD_32
			LOAD_16
			LOAD_8
            FMA_LOAD_8
        }
        STORE_8
    }
}

void kernel_pd_64_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*16; i++) {
            LOAD_32
			LOAD_16
			LOAD_8
            FMA_LOAD_8
        }
        STORE_8
    }
}

void kernel_pd_32_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*32; i++) {
            LOAD_16
			LOAD_8
            FMA_LOAD_8
        }
        STORE_8
    }
}

void kernel_pd_16_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*64; i++) {
            LOAD_8
            FMA_LOAD_8
        }
        STORE_8
    }
}

void kernel_pd_8_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*128; i++) {
            FMA_LOAD_8
        }
        STORE_8
    }
}

void kernel_pd_8_16(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*64; i++) {
            FMA_LOAD_8
			FMA_8
        }
        STORE_8
    }
}

void kernel_pd_8_32(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*32; i++) {
			FMA_LOAD_8
            FMA_16
			FMA_8
        }
        STORE_8
    }
}

void kernel_pd_8_64(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*16; i++) {
            FMA_LOAD_8
			FMA_32
			FMA_16
			FMA_8
        }
        STORE_8
    }
}

void kernel_pd_8_128(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*8; i++) {
            FMA_LOAD_8
            FMA_64 FMA_32
			FMA_16 FMA_8
        }
        STORE_8
    }
}

void kernel_pd_8_256(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT*4; i++) {
            FMA_LOAD_8
            FMA_64 FMA_64
            FMA_64 FMA_32
			FMA_16 FMA_8
        }
        STORE_8
    }
}
