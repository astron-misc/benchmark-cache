#include "common.h"

void kernel_pd_1024_8(void *ptr);
void kernel_pd_512_8(void *ptr);
void kernel_pd_256_8(void *ptr);
void kernel_pd_128_8(void *ptr);
void kernel_pd_64_8(void *ptr);
void kernel_pd_32_8(void *ptr);
void kernel_pd_16_8(void *ptr);
void kernel_pd_8_8(void *ptr);
void kernel_pd_8_16(void *ptr);
void kernel_pd_8_32(void *ptr);
void kernel_pd_8_64(void *ptr);
void kernel_pd_8_128(void *ptr);
void kernel_pd_8_256(void *ptr);
