#include "common.h"

void kernel_int_1024_8(void *ptr);
void kernel_int_512_8(void *ptr);
void kernel_int_256_8(void *ptr);
void kernel_int_128_8(void *ptr);
void kernel_int_64_8(void *ptr);
void kernel_int_32_8(void *ptr);
void kernel_int_16_8(void *ptr);
void kernel_int_8_8(void *ptr);
void kernel_int_8_16(void *ptr);
void kernel_int_8_32(void *ptr);
void kernel_int_8_64(void *ptr);
void kernel_int_8_128(void *ptr);
void kernel_int_8_256(void *ptr);
