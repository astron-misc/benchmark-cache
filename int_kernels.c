#include "int_kernels.h"

#define ADD_8 \
    asm ("vpaddd %1, %1, %0" :  "=x" (a) : "x" (m0), "0" (a)); \
    asm ("vpaddd %1, %1, %0" :  "=x" (b) : "x" (m1), "0" (b)); \
    asm ("vpaddd %1, %1, %0" :  "=x" (c) : "x" (m2), "0" (c)); \
    asm ("vpaddd %1, %1, %0" :  "=x" (d) : "x" (m3), "0" (d)); \
    asm ("vpaddd %1, %1, %0" :  "=x" (e) : "x" (m4), "0" (e)); \
    asm ("vpaddd %1, %1, %0" :  "=x" (f) : "x" (m5), "0" (f)); \
    asm ("vpaddd %1, %1, %0" :  "=x" (g) : "x" (m6), "0" (g)); \
    asm ("vpaddd %1, %1, %0" :  "=x" (h) : "x" (m7), "0" (h));

#define INIT \
    vector_ps a, b, c, d, e, f, g, h; \
    a = set_ps; \
    b = set_ps; \
    c = set_ps; \
    d = set_ps; \
    e = set_ps; \
    f = set_ps; \
    g = set_ps; \
    h = set_ps; \
    vector_ps m0, m1, m2, m3, m4, m5, m6, m7; \
    int* m0_ptr = (int *) ptr + 0 * VECTOR_LENGTH; \
    int* m1_ptr = (int *) ptr + 1 * VECTOR_LENGTH; \
    int* m2_ptr = (int *) ptr + 2 * VECTOR_LENGTH; \
    int* m3_ptr = (int *) ptr + 3 * VECTOR_LENGTH; \
    int* m4_ptr = (int *) ptr + 4 * VECTOR_LENGTH; \
    int* m5_ptr = (int *) ptr + 5 * VECTOR_LENGTH; \
    int* m6_ptr = (int *) ptr + 6 * VECTOR_LENGTH; \
    int* m7_ptr = (int *) ptr + 7 * VECTOR_LENGTH;

#define LOAD_8 \
    asm ("vmovaps %1, %0" : "=x" (m0) : "m" (*m0_ptr)); \
    asm ("vmovaps %1, %0" : "=x" (m1) : "m" (*m1_ptr)); \
    asm ("vmovaps %1, %0" : "=x" (m2) : "m" (*m2_ptr)); \
    asm ("vmovaps %1, %0" : "=x" (m3) : "m" (*m3_ptr)); \
    asm ("vmovaps %1, %0" : "=x" (m4) : "m" (*m4_ptr)); \
    asm ("vmovaps %1, %0" : "=x" (m5) : "m" (*m5_ptr)); \
    asm ("vmovaps %1, %0" : "=x" (m6) : "m" (*m6_ptr)); \
    asm ("vmovaps %1, %0" : "=x" (m7) : "m" (*m7_ptr)); \

#define STORE_8 \
    asm ("vmovntps %1, %0" : "=m" (*m0_ptr) : "x" (m0)); \
    asm ("vmovntps %1, %0" : "=m" (*m1_ptr) : "x" (m1)); \
    asm ("vmovntps %1, %0" : "=m" (*m2_ptr) : "x" (m2)); \
    asm ("vmovntps %1, %0" : "=m" (*m3_ptr) : "x" (m3)); \
    asm ("vmovntps %1, %0" : "=m" (*m4_ptr) : "x" (m4)); \
    asm ("vmovntps %1, %0" : "=m" (*m5_ptr) : "x" (m5)); \
    asm ("vmovntps %1, %0" : "=m" (*m6_ptr) : "x" (m6)); \
    asm ("vmovntps %1, %0" : "=m" (*m7_ptr) : "x" (m7)); \

void kernel_int_1024_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT/8; i++) {
            LOAD_64 LOAD_64 LOAD_64 LOAD_64
            LOAD_64 LOAD_64 LOAD_64 LOAD_64
            LOAD_64 LOAD_64 LOAD_64 LOAD_64
            LOAD_64 LOAD_64 LOAD_64 LOAD_64
            ADD_8
        }
        STORE_8
    }
}
void kernel_int_512_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT/4; i++) {
            LOAD_64 LOAD_64
            LOAD_64 LOAD_64
            LOAD_64 LOAD_64
            LOAD_64 LOAD_64
            ADD_8
        }
        STORE_8
    }
}

void kernel_int_256_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT/2; i++) {
            LOAD_64 LOAD_64
            LOAD_64 LOAD_64
            ADD_8
        }
        STORE_8
    }
}

void kernel_int_128_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            LOAD_64
            LOAD_64
            ADD_8
        }
        STORE_8
    }
}

void kernel_int_64_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            LOAD_64
            ADD_8
        }
        STORE_8
    }
}

void kernel_int_32_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            LOAD_32
            ADD_8
        }
        STORE_8
    }
}

void kernel_int_16_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            LOAD_16
            ADD_8
        }
        STORE_8
    }
}

void kernel_int_8_8(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            LOAD_8
            ADD_8
        }
        STORE_8
    }
}

void kernel_int_8_16(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            LOAD_8
            ADD_16
        }
        STORE_8
    }
}

void kernel_int_8_32(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            LOAD_8
            ADD_32
        }
        STORE_8
    }
}

void kernel_int_8_64(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT; i++) {
            LOAD_8
            ADD_64
        }
        STORE_8
    }
}

void kernel_int_8_128(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT/2; i++) {
            LOAD_8
            ADD_64
            ADD_64
        }
        STORE_8
    }
}

void kernel_int_8_256(void * __restrict__ ptr) {
    INIT
    #pragma omp parallel
    {
        #pragma omp for schedule(static)
        for (unsigned long long i = 0; i < COUNT/4; i++) {
            LOAD_8
            ADD_64 ADD_64
            ADD_64 ADD_64
        }
        STORE_8
    }
}
